from django.db import models

# Create your models here.

class User(models.Model):
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    is_admin = models.BooleanField(default=False)
    last_name = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100)
    date_of_birth = models.DateField()
    phone_number = models.CharField(max_length=10)
    school = models.CharField(max_length=100)
    school_year = models.CharField(max_length=100)
    school_level = models.CharField(max_length=100)
    speciality = models.ManyToManyField('Speciality', related_name='users')
    global_progress = models.IntegerField(default=0, null=True)
    daily_progress = models.IntegerField(default=0, null=True)
    github_link = models.CharField(max_length=100, null=True)
    date_created = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.first_name + " " + self.last_name
    
class Subject(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100, null=True)
    date_created = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name
    
class Exercise(models.Model):
    STATUS = (
        ('En attente', 'En attente'),
        ('En cours', 'En cours'),
        ('Terminé', 'Terminé'),
    )
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100, null=True)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    hints = models.CharField(max_length=255, null=True)
    bootcamp_day = models.IntegerField(default=0, null=True)
    speciality = models.ForeignKey('Speciality', on_delete=models.CASCADE)
    passed_by = models.ManyToManyField(User, related_name='passed_exercises', blank=True)
    status = models.CharField(max_length=100, null=True, choices=STATUS)
    date_created = models.DateField(auto_now_add=True)
    


    def __str__(self):
        return self.name
    
class Speciality(models.Model):
    name = models.CharField(max_length=100)
    date_created = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name
