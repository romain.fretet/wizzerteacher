from django.urls import path
from . import views

urlpatterns = [
    path('', views.home),
    path('profile/', views.profile),
    path('lessons/', views.lessons),
    path('exercises/', views.exercises),
    path('login/', views.login),
    path('register/', views.register),
]
