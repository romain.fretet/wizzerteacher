from django.contrib import admin

# Register your models here.

from .models import User, Subject, Exercise, Speciality

admin.site.register(User)
admin.site.register(Subject)
admin.site.register(Exercise)
admin.site.register(Speciality)
